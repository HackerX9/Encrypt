package com.palfund.encrypt;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

public class MainActivity extends AppCompatActivity {
    private boolean mBase64ed = false;
    private boolean mURLEncoded = false;
    private boolean mAESed = false;
    private boolean mRSAed = false;
    //秘钥--AES的秘钥至少16个字节,可以是16的整数倍.
    private String key_AES = "0123456789ABCDEF";
    private EditText mEditText;
    private TextView mTextView_encode;
    private TextView mTextView_decode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEditText = (EditText) findViewById(R.id.editText);
        mTextView_encode = (TextView) findViewById(R.id.textView_encode);
        mTextView_decode = (TextView) findViewById(R.id.textView_decode);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, 1, 1, "Base64");
        menu.add(Menu.NONE, 2, 1, "URLEncode");
        menu.add(Menu.NONE, 3, 1, "MD5");
        menu.add(Menu.NONE, 4, 1, "AES");
        menu.add(Menu.NONE, 5, 1, "RSA");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // 获取需要编码的字符串
        String string = mEditText.getText().toString();
        switch (item.getItemId()) {
            case 1:
                try {
                    base64(string);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    URLEncode(string);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                try {
                    md5(string);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case 4:
                try {
                    // 对称加密
                    aes(string);
                } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case 5:
                // 非对称加密
                try {
                    rsa(string);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    // 非对称加密
    private void rsa(String string) throws UnsupportedEncodingException {
        if (!mRSAed) {
            //加密后的结果
            byte[] encrypt = RSAUtil.encrypt(string.getBytes("UTF-8"));
            //进行Base64编码
            byte[] encode = Base64.encode(encrypt, Base64.DEFAULT);
            mTextView_encode.setText(new String(encode));
        } else {
            //进行Base64解码
            byte[] decode = Base64.decode(mTextView_encode.getText().toString().getBytes("UTF-8"), Base64.DEFAULT);
            //解密
            byte[] decrypt = RSAUtil.decrypt(decode);
            mTextView_decode.setText(new String(decrypt));
        }
        mRSAed = !mRSAed;
    }

    // 对称加密
    private void aes(String string) throws IllegalBlockSizeException, UnsupportedEncodingException {
        if (!mAESed) {
            //加密后的结果
            byte[] encrypt = AESUtils.encrypt(Cipher.ENCRYPT_MODE, key_AES, string.getBytes
                    ("UTF-8"));
            //进行Base64编码
            byte[] encode = Base64.encode(encrypt, Base64.DEFAULT);
            mTextView_encode.setText(new String(encode));
        } else {
            // 解码
            byte[] decode = Base64.decode(mTextView_encode.getText().toString().getBytes("UTF-8")
                    , Base64.DEFAULT);
            // 解密
            byte[] encrypt = AESUtils.encrypt(Cipher.DECRYPT_MODE, key_AES, decode);
            mTextView_decode.setText(new String(encrypt));
        }
        mAESed = !mAESed;
    }

    private void md5(String string) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        //得到MD5的实例
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        // 进行加密
        byte[] digest = md5.digest(string.getBytes("UTF-8"));
        // 要先用Base64进行编码,然后再进行展示
        byte[] encode = Base64.encode(digest, Base64.DEFAULT);
        mTextView_encode.setText(new String(encode));
    }

    private void URLEncode(String string) throws UnsupportedEncodingException {
        if (!mURLEncoded) {
            String encode = URLEncoder.encode(string, "UTF-8");
            mTextView_encode.setText(encode);
        } else {
            String decode = URLDecoder.decode(mTextView_encode.getText().toString(), "UTF-8");
            mTextView_decode.setText(decode);
        }
        mURLEncoded = !mURLEncoded;
    }

    private void base64(String string) throws UnsupportedEncodingException {
        if (!mBase64ed) {
            // 编码
            byte[] encode = Base64.encode(string.getBytes("UTF-8"), Base64.DEFAULT);
            /**
             无论是编码还是解码都会有一个参数Flags
             DEFAULT 这个参数是默认，使用默认的方法来加密
             NO_PADDING 这个参数是略去加密字符串最后的”=”
             NO_WRAP 这个参数意思是略去所有的换行符（设置后CRLF就没用了）
             CRLF 这个参数看起来比较眼熟，它就是Win风格的换行符，意思就是使用CR LF这一对作为一行的结尾而不是Unix风格的LF
             URL_SAFE 这个参数意思是加密时不使用对URL和文件名有特殊意义的字符来作为加密字符，具体就是以-和_取代+和/
             */
            mTextView_encode.setText(new String(encode));

        } else {
            // 解码
            byte[] decode = Base64.decode(mTextView_encode.getText().toString().getBytes("UTF-8")
                    , Base64.DEFAULT);
            mTextView_decode.setText(new String(decode));
        }
        mBase64ed = !mBase64ed;
    }
}

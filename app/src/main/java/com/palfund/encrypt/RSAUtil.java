package com.palfund.encrypt;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by clvc on 2017/8/13.
 * 真正的速度是看不见的 !
 * Today is today , we will go !
 */

public class RSAUtil {
    //模
    private static final String MODULUS =
            "100631058000714094813874361191853577129731636346684218206605779824931626830750623070803100189781211343851763275329364056640619755337779928985272486091431384128027213365372009648233171894708338213168824861061809490615593530405056055952622249066180336803996949444124622212096805545953751253607916170340397933039";
    //公钥指数.一个指数.e=2的16次方+1
    private static final String PUB_KEY = "65537";
    //私钥
    private static final String PRI_KEY =
            "26900155715313643087786516528374548998821559381075740707715132776187148793016466508650068087107695523642202737697714709374658856733792614490943874205956727606674634563665154616758939576547663715234643273055658829482813503959459653708062875625210008961239643775661357655599312857249418610810177817213648575161";

    //加密方法
    public static byte[] encrypt(byte[] data) {
        try {
            //生成RSA算法
            Cipher cipher = Cipher.getInstance("RSA");
            //创键密钥的工厂
            KeyFactory factory = KeyFactory.getInstance("RSA");
            //设置公钥
            RSAPublicKeySpec keySpec = new RSAPublicKeySpec(new BigInteger(MODULUS), new
                    BigInteger(PUB_KEY));
            //创建公钥
            PublicKey publicKey = factory.generatePublic(keySpec);
            //初始化
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            //加密
            return cipher.doFinal(data);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException |
                IllegalBlockSizeException | InvalidKeyException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }

    //解密方法
    public static byte[] decrypt(byte[] data) {
        try {
            //生成RSA算法
            Cipher cipher = Cipher.getInstance("RSA");
            //创键密钥的工厂
            KeyFactory factory = KeyFactory.getInstance("RSA");
            //设置私钥
            RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(new BigInteger(MODULUS), new
                    BigInteger(PRI_KEY));
            //创建公钥
            PrivateKey privateKey = factory.generatePrivate(keySpec);
            //初始化
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            //解密
            return cipher.doFinal(data);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException |
                IllegalBlockSizeException | InvalidKeyException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }
}


package com.palfund.encrypt;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by clvc on 2017/8/13.
 * 真正的速度是看不见的 !
 * Today is today , we will go !
 */

public class AESUtils {
    static final String AES_FLAG = "AES/ECB/PKCS5Padding";
    //加密和解密的方法.
    static byte[] encrypt(int mode, String key, byte[] data) throws IllegalBlockSizeException {
        try {
            //得到AES加密算法
            Cipher cipher = Cipher.getInstance(AES_FLAG);
            //初始化密钥
            SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "AES");
            //初始化加密算法
            cipher.init(mode, keySpec);
            //进行加密
            return cipher.doFinal(data);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
                BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
